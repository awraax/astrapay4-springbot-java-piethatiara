package com.electricalweb.daos;

import com.electricalweb.entities.Product;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class ProductDao {

    private EntityManager entityManager;
    private EntityTransaction entityTransaction;

    public ProductDao(EntityManager entityManager){
        this.entityManager = entityManager;
        this.entityTransaction = this.entityManager.getTransaction();
    }

    public void persist (String name, int categoryId, double price){
        beginTransaction();
        Product product = new Product(name, categoryId, price);
        entityManager.persist(product);
        commitTransaction();

    }

    public Product find(long id){
        return  entityManager.find(Product.class, id);
    }

    public void update (long id, String name, int categoryId, double price){
        beginTransaction();
        Product product = entityManager.find(Product.class, id);
        product.setName(name);
        product.setCategoryId(categoryId);
        product.setPrice(price);
        entityManager.merge(product);
        commitTransaction();
    }

    public void remove (long id){
        beginTransaction();
        Product product = entityManager.find(Product.class, id);
        entityManager.remove(product);
        commitTransaction();
    }

    private void beginTransaction() {
        try{
            entityTransaction.begin();
        }catch (IllegalStateException e){
            rollbackTranscation();
        }
    }

    private void commitTransaction(){
        try{
            entityTransaction.commit();
            entityManager.close();
        }catch (IllegalStateException e){
            rollbackTranscation();
        }
    }

    private void rollbackTranscation(){
        try {
            entityTransaction.rollback();
        }catch (IllegalStateException e){
            e.printStackTrace();
        }
    }
}
