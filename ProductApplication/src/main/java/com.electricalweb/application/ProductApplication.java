package com.electricalweb.application;

import com.electricalweb.daos.ProductDao;
import com.electricalweb.entities.Product;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ProductApplication {
    private final ProductDao productDao;
    private final BufferedReader userInputReader;

    public static void main(String[] args) throws IOException {
        EntityManager entityManager = Persistence
                .createEntityManagerFactory("product-unit")
                .createEntityManager();
        ProductDao productDao = new ProductDao(entityManager);
        BufferedReader userInputReader =
                new BufferedReader(new InputStreamReader(System.in));
        new ProductApplication(productDao, userInputReader).run();
    }

    public ProductApplication(ProductDao productDao, BufferedReader userInputReader) {
        this.productDao = productDao;
        this.userInputReader = userInputReader;
    }

    private void run() throws IOException{
        int input = 0;
            System.out.println(
                            "1) Insert a new user : "
                            + "\n2) Find a user : "
                            + "\n3) Edit a user : "
                            + "\n4) Delete a user : "
                            + "\nChoose the option : ");
            input = Integer.parseInt(userInputReader.readLine());

            switch (input) {
                case 1:
                    persistNewProduct();
                    break;
                case 2:
                    fetchExistingProduct();
                    break;
                case 3:
                    updateExistingProduct();
                    break;
                case 4:
                    removeExistingProduct();
                    break;
            }

    }

    private void persistNewProduct() throws IOException {
        String name = requestStringInput("the name of the product");
        int categoryId = requestIntegerInput("the categoryId of the product");
        double price = requestDoubleInput("the price of the product");
        productDao.persist(name, categoryId, price);
    }

    private void fetchExistingProduct() throws IOException {
        int id = requestIntegerInput("the product id");
        Product product = productDao.find(id);
        System.out.print("Name : " + product.getName() + "Category Id : " + product.getCategoryId() + "Price : " +product.getPrice());
    }

    private void updateExistingProduct() throws IOException {
        int id = requestIntegerInput("the product Id");
        String name = requestStringInput("the name of the user");
        int categoryId = requestIntegerInput("the categoryId of the product");
        double price = requestDoubleInput("the price of the product");
        productDao.update(id, name, categoryId, price);
    }

    private void removeExistingProduct() throws IOException {
        int id = requestIntegerInput("the product Id");
        productDao.remove(id);
    }

    private String requestStringInput(String request) throws IOException {
        System.out.printf("Enter %s: ", request);
        return userInputReader.readLine();
    }

    private int requestIntegerInput(String request) throws IOException {
        System.out.printf("Enter %s: ", request);
        return Integer.parseInt(userInputReader.readLine());
    }

    private double requestDoubleInput (String request) throws IOException {
        System.out.printf("Enter %s: ",request);
        return Double.parseDouble(userInputReader.readLine());
    }
}
