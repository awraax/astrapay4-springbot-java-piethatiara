package com.codeflex.springboot.service;

import com.codeflex.springboot.model.Product;
import org.springframework.stereotype.Repository;
import java.util.List;


@Repository("productServiceImpl")
public interface ProductServiceImpl {
    Product findById(long id);

    Product findByName(String name);

    Product saveProduct(Product product);

    void updateProduct(Product product, long id);

    void deleteProductById(long id);

    List<Product> findAllProducts();

    void deleteAllProducts();

    boolean isProductExist(Product product);
//    int count();
//
//    int save(Product product);
//
//    int update(Product Product);
//
//    int deleteById(Long id);
//
//    List<Product> findAll();
//
//    List<Product> findByNameAndPrice(String name, double price);
//
//    Optional<Product> findById(Long id);
//
//    String findNameById(Long id);
//
//    int[] batchInsert(List<Product> product);
//
//    int[][] batchInsert(List<Product> product, int batchSize);
//
//    int[] batchUpdate(List<Product> product);
//
//    int[][] batchUpdate(List<Product> product, int batchSize);
//
//    void saveImage(Long productId, File image);
//
//    List<Map<String, InputStream>> findImageByBookId(Long productId);

}
