package com.codeflex.springboot.service;

import com.codeflex.springboot.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ParameterizedPreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.AbstractLobCreatingPreparedStatementCallback;
import org.springframework.jdbc.support.lob.LobCreator;
import org.springframework.jdbc.support.lob.LobHandler;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository("productServiceImpl")
public class ProductService implements  ProductServiceImpl {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Product findById(long id) {
        return jdbcTemplate.queryForObject(
                "select * from product where id = ?",
                new Object[]{id},
                (rs, rowNum) ->
                        new Product(
                                rs.getLong("id"),
                                rs.getString("name"),
                                rs.getInt("categoryId"),
                                rs.getDouble("price")
                        )
        );
    }

    @Override
    public Product findByName(String name) {
        return jdbcTemplate.queryForObject(
                "select * from product where name = ?",
                new Object[]{name},
                (rs, rowNum) ->
                        new Product(
                                rs.getLong("id"),
                                rs.getString("name"),
                                rs.getInt("categoryId"),
                                rs.getDouble("price")
                        )
        );
    }

    @Override
    public Product saveProduct(Product product) {
        int ret = jdbcTemplate.update(
                "insert into product (name, categoryId, price) values(?,?,?)",
                product.getName(), product.getCategoryId(), product.getPrice());
        System.out.println(ret + " record inserted");
        return product;
    }

    @Override
    public void updateProduct(Product product, long id) {
        int ret = jdbcTemplate.update(
                "update product set name = ?, categoryId = ?, price = ? where id = ?",
                product.getName(), product.getCategoryId(), product.getPrice(), product.getId());
        System.out.println(ret + " record updated");
    }

    @Override
    public void deleteProductById(long id) {
        int ret = jdbcTemplate.update(
                "delete from product where id = ?",
                id);
        System.out.println(ret + " record deleted");
    }

    @Override
    public List<Product> findAllProducts() {
        return jdbcTemplate.query(
                "select * from product",
                (rs, rowNum) ->
                        new Product(
                                rs.getLong("id"),
                                rs.getString("name"),
                                rs.getInt("categoryId"),
                                rs.getDouble("price")
                        )
        );
    }

    @Override
    public void deleteAllProducts() {
        int ret = jdbcTemplate.update(
                "delete from product");
        System.out.println(ret + " record(s) deleted");
    }

    @Override
    public boolean isProductExist(Product product) {
        return false;
    }


//    @Override
//    public int count() {
//        return jdbcTemplate
//                .queryForObject("Select count (*) from product", Integer.class);
//    }
//
//    @Override
//    public int save(Product product) {
//        int ret = jdbcTemplate.update(
//                "insert into product (name, categoryId, price) values (?,?,?)",
//                product.getName(), product.getCategoryId(), product.getPrice()
//        );
//        return ret;
//    }
//
//    @Override
//    public int update(Product product) {
//        return jdbcTemplate.update(
//                "update product set price = ? where id = ?",
//                product.getPrice(), product.getId()
//        );
//    }
//
//    @Override
//    public int deleteById(Long id) {
//        return jdbcTemplate.update(
//                "delete from product where id = ?", id
//        );
//    }
//
//    @Override
//    public List<Product> findAll() {
//        return jdbcTemplate.query(
//                "select * from product",
//                (rs, rowNum)->
//                        new Product(
//                                rs.getString("name"),
//                                rs.getInt("categoryId"),
//                                rs.getDouble("price")
//                        )
//        );
//    }
//
//    @Override
//    public List<Product> findByNameAndPrice(String name, double price) {
//        return jdbcTemplate.query(
//                "select * from product where name like ? and price <= ?",
//                new Object[]{"%" + name + "%", price},
//                (rs, rowNum) ->
//                        new Product(
//                                rs.getLong("id"),
//                                rs.getString("name"),
//                                rs.getDouble("price")
//                        )
//        );
//    }
//
//    @Override
//    public Optional<Product> findById(Long id) {
//        return jdbcTemplate.queryForObject(
//                "select * from product where id = ?",
//                new Object[]{id},
//                (rs, rowNum) ->
//                        Optional.of(new Product(
//                                rs.getLong("id"),
//                                rs.getString("name"),
//                                rs.getDouble("price")
//                        ))
//        );
//    }
//
//    @Override
//    public String findNameById(Long id) {
//        return jdbcTemplate.queryForObject(
//                "select name from product where id = ?",
//                new Object[]{id},
//                String.class
//        );
//    }
//
//    @Override
//    public int[] batchInsert(List<Product> product) {
//        return this.jdbcTemplate.batchUpdate(
//                "insert into product (name, categoryId, price) values(?,?,?)",
//                new BatchPreparedStatementSetter() {
//
//                    public void setValues(PreparedStatement preparedStatement, int i) throws SQLException {
//                        preparedStatement.setString(1, product.get(i).getName());
//                        preparedStatement.setInt(2, product.get(i).getCategoryId());
//                        preparedStatement.setDouble(3, product.get(1).getPrice());
//                    }
//                    public int getBatchSize() {
//                        return product.size();
//                    }
//                });
//    }
//    @Transactional
//    @Override
//    public int[][] batchInsert(List<Product> product, int batchSize) {
//        int[][] updateCounts = jdbcTemplate.batchUpdate(
//                "insert into product (name, categoryId, price) values (?,?,?)",
//                product,
//                batchSize,
//                new ParameterizedPreparedStatementSetter<Product>() {
//                    public void setValues(PreparedStatement preparedStatement, Product product) throws SQLException {
//                        preparedStatement.setString(1, product.getName());
//                        preparedStatement.setInt(2, product.getCategoryId());
//                        preparedStatement.setDouble(3, product.getPrice());
//                    }
//                });
//        return updateCounts;
//    }
//
//    @Override
//    public int[] batchUpdate(List<Product> product) {
//        return  this.jdbcTemplate.batchUpdate("update product set price = ? where id = ",
//                new BatchPreparedStatementSetter() {
//
//                public void setValues(PreparedStatement preparedStatement, int i) throws SQLException {
//                    preparedStatement.setDouble(1, product.get(i).getPrice());
//                    preparedStatement.setLong(2, product.get(i).getId());
//                }
//
//
//                public int getBatchSize() {
//                        return product.size();
//                    }
//                });
//    }
//
//    @Override
//    public int[][] batchUpdate(List<Product> product, int batchSize) {
//        int[][] updateCounts = jdbcTemplate.batchUpdate(
//                "update product set price = ? where id = ?",
//                product,
//                batchSize,
//                new ParameterizedPreparedStatementSetter<Product>() {
//                    public void setValues(PreparedStatement preparedStatement, Product product) throws SQLException {
//                        preparedStatement.setDouble(1, product.getPrice());
//                        preparedStatement.setLong(2, product.getId());
//                    }
//                });
//        return updateCounts;
//    }
//
//    @Override
//    public void saveImage(Long productId, File image) {
//    }
//
//    @Override
//    public List<Map<String, InputStream>> findImageByBookId(Long productId) {
//        return null;
//    }
}
